import { setContext } from 'apollo-link-context';
import { LocalStorage } from 'quasar';
import { LS_AUTH_TOKEN_NAME } from 'src/js/constants';

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = LocalStorage.getItem(LS_AUTH_TOKEN_NAME);
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : '',
    },
  };
});

export function apolloClientBeforeCreate({ apolloClientConfigObj }) {
  const { link } = apolloClientConfigObj;
  apolloClientConfigObj.link = authLink.concat(link);
}

export function apolloClientAfterCreate(/* { apolloClient, app, router, store, ssrContext, urlPath, redirect } */) {
  // if needed you can modify here the created apollo client
}
