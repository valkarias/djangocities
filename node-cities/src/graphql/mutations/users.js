import gql from 'graphql-tag';

export const login = gql`mutation ($data: LoginInput!) {
  login (data: $data) {
    status
    error
    token
  }
}`;
