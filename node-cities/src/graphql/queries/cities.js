import gql from 'graphql-tag';

export const allCities = gql`query {
  allCities {
    name
    description
    slug
  }
}`;
