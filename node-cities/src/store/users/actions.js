import { LocalStorage } from 'quasar';
import { LS_AUTH_TOKEN_NAME } from 'src/js/constants';

const jwtDecode = require('jwt-decode');

export function readAuthTokenFromStorage(context) {
  const token = LocalStorage.getItem(LS_AUTH_TOKEN_NAME);
  if (token !== null) {
    const authToken = jwtDecode(token);
    context.commit('setAuthToken', authToken);
  }
}
