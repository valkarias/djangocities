export function decodedAuthToken(state) {
  try {
    const { authToken } = state;

    // check that the token is valid
    if (authToken === null) return null;
    const timeStamp = Math.floor(new Date().getTime() / 1000);
    if (timeStamp > authToken.exp) return null;

    // use Object.prototype otherwise ESLint raises a no-prototype-builtins error
    // as authToken.hasOwnProperty may be a security vulnerability
    if (Object.prototype.hasOwnProperty.call(authToken, 'user')) return authToken;

    // this should never be reached
    return null;
  } catch (error) {
    return null;
  }
}
