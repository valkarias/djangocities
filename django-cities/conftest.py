# These are pytest fixtures, they need to be imported
# flake8: noqa
from tests.fixtures import (
    admin_user,
    regular_user,
    area51,
    sunset_strip,
    alien_slugs,
    alien_kitchen,
    rock_land,
)
