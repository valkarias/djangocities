echo $(date) - waiting for db
python pgwait.py

echo $(date) - applying migrations
python manage.py migrate

echo $(date) - starting dev server
python manage.py runserver 0.0.0.0:8000