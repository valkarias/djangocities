from enum import Enum

from django.conf import settings
from django.db import models

User = settings.AUTH_USER_MODEL


class HTMLVersions(Enum):
    HTML1 = "1"
    HTML2 = "2"


HTML_VERSION_CHOICES = [(h.value, h.name) for h in HTMLVersions]


class City(models.Model):
    name = models.CharField(max_length=40, unique=True)
    description = models.CharField(max_length=250)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = "cities"

    def __str__(self):
        return self.name


class WebSite(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name="websites")
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30, unique=True)
    address = models.IntegerField(default=0)
    deleted = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["city", "address"], name="unique_address"),
        ]

    def __str__(self):
        return f"{self.name} | {self.city.name}"

    def generate_address(self) -> int:
        if self.address != 0:  # only generate address if it's default
            return self.address
        if (
            largest_address := WebSite.objects.filter(city=self.city)
            .aggregate(models.Max("address"))
            .get("address__max", None)
        ) is None:
            return 1
        else:
            return largest_address + 1

    def save(self, *args, **kwargs):
        self.name = self.name.title()
        self.address = self.generate_address()
        super().save(*args, **kwargs)


class WebPage(models.Model):
    website = models.ForeignKey(
        WebSite, on_delete=models.CASCADE, related_name="web_pages",
    )
    name = models.CharField(max_length=30, unique=False)
    html_content = models.TextField()
    html_version = models.CharField(max_length=4, choices=HTML_VERSION_CHOICES)
    view_count = models.IntegerField()

    def __str__(self):
        return f"{self.name} | {self.website.name}"

    def name_is_unique(self):
        return self.name not in [x.name for x in self.website.web_pages.all()]

    def save(self, *args, **kwargs):
        # html_content validation should go here
        self.name = self.name.lower()
        if self.name_is_unique():
            super().save(*args, **kwargs)
