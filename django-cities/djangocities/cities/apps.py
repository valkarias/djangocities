from django.apps import AppConfig


class CitiesConfig(AppConfig):
    name = "djangocities.cities"
