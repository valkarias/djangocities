from django.contrib import admin
from .models import City, WebPage, WebSite


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ("name", "description")
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(WebSite)
admin.site.register(WebPage)
