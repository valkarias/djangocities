from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ImproperlyConfigured
from django.db import models

import jwt


class User(AbstractUser):
    pass


class Token(models.Model):
    """Abstract model class to be reused in AuthToken, and eventually in RefreshToken models."""

    expiration = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # Most servers limit headers to 8KB, so don't use more than half of that
    token = models.CharField(max_length=4096)

    class Meta:
        abstract = True

    @staticmethod
    def get_expiration():
        """
        Calculate the expiration time based on settings.
        This is an abstract method to override.
        """
        raise NotImplementedError("This method is abstract.")

    def encode_token(self):
        """This is an abstract method to override."""
        raise NotImplementedError("This method is abstract.")


class AuthToken(Token):
    @staticmethod
    def get_expiration() -> datetime:
        """Calculate the expiration time for Auth tokens."""
        token_delta = settings.JWT_AUTH_TOKEN_DELTA
        if not isinstance(token_delta, timedelta):
            raise ImproperlyConfigured(
                "JWT_AUTH_TOKEN_DELTA must be an instance of datetime.timedelta."
            )
        return datetime.now() + token_delta

    def set_expiration(self):
        self.expiration = self.get_expiration()

    def encode_token(self):
        """Encode a user Authentication token."""
        if not self.expiration:
            raise ValueError(
                "Please set the token expiration before encoding the token."
            )
        if not self.user or not isinstance(self.user, User):
            raise ValueError("Please set the token user before encoding the token.")

        encode_time = datetime.utcnow()
        payload = {
            "exp": self.expiration,
            "nbf": encode_time,
            "iat": encode_time,
            "user": self.user.username,
            "is_admin": self.user.is_superuser,
        }
        encoded_token = jwt.encode(
            payload, settings.SECRET_KEY, algorithm=settings.JWT_ALGORITHM,
        )
        self.token = encoded_token
