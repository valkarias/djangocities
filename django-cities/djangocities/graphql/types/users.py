from ariadne import ObjectType

from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from djangocities.users.models import AuthToken, User


user = ObjectType("User")


def generate_user_auth_token(user=None):
    if user is None:
        raise ValueError("User cannot be none.")

    auth_token = AuthToken(user=user)
    auth_token.set_expiration()
    auth_token.encode_token()
    auth_token.save()
    return auth_token


@user.field("websites")
def resolve_websites(obj, info):
    return obj.website_set.all()


def resolve_login(_, info, data):
    request = info.context["request"]
    user = authenticate(username=data["username"], password=data["password"])

    if user is None:
        return {
            "status": False,
            "error": "Invalid username or password",
            "user": None,
        }

    # User is valid
    login(request, user)

    auth_token = generate_user_auth_token(user)
    return {
        "status": True,
        "token": auth_token.token,
    }


def resolve_register(_, info, data):
    """Validate registration data, create a User if valid, login new user and return a token."""
    if not settings.REGISTRATION_ENABLED:
        return {
            "status": False,
            "error": "User registration is currently disabled",
        }

    request = info.context["request"]

    failures = []

    # username already exists
    if User.objects.filter(username=data["username"]):
        failures.append("Username already exists")

    # password verification fails
    try:
        validate_password(data["password"])
    except ValidationError:
        failures.append("Invalid password")

    # email verification fails
    try:
        validate_email(data["email"])
    except ValidationError:
        failures.append("Invalid email")

    if failures:
        return {
            "status": False,
            "error": ", ".join(failures),
        }

    user = User.objects.create_user(
        username=data["username"], password=data["password"], email=data["email"]
    )

    # user creation fails after password and email checks
    if user is None:
        return {
            "status": False,
            "error": "Could not create user",
        }

    login(request, user)
    auth_token = generate_user_auth_token(user)

    return {
        "status": user is not None,
        "token": str(auth_token.token),
    }
