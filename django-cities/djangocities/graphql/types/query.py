from ariadne import QueryType

from djangocities.cities.models import City, WebSite
from djangocities.users.models import User

query = QueryType()


@query.field("allCities")
def resolve_all_cities(root, info):
    return City.objects.all()


@query.field("allWebSites")
def resolve_all_web_sites(root, info):
    return WebSite.objects.all()


@query.field("allUsers")
def resolve_all_users(root, info):
    return User.objects.all()
