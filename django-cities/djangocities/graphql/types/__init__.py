from .cities import city, web_site
from .mutation import mutation
from .query import query
from .users import user

types = [
    city,
    web_site,
    mutation,
    query,
    user,
]
