from ariadne import MutationType

from .users import resolve_login, resolve_register

mutation = MutationType()
mutation.set_field("login", resolve_login)
mutation.set_field("register", resolve_register)
