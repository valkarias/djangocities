from ariadne import ObjectType

city = ObjectType("City")
web_site = ObjectType("WebSite")


@city.field("websites")
def resolve_websites(obj, _):
    return obj.websites.all()
