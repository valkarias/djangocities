"""Token utilities."""

from django.conf import settings
import jwt


def decode_token(token, verify=True):
    try:
        return jwt.decode(token, settings.SECRET_KEY, algorithm=settings.JWT_ALGORITHM)
    except jwt.ExpiredSignatureError:
        # Raise an exception we are actually catching
        raise ValueError
