from django.contrib.auth import authenticate
from djangocities.utils.tokens import decode_token


class AuthTokenMiddleware:
    """Check request for Authorization headers containing a bearer token."""
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            auth = request.headers["Authorization"]
            _type, token = auth.split(" ", 1)
            if _type.lower() != "bearer":
                raise ValueError
            decoded = decode_token(token)
            request._auth_token = decoded
            if request.user.is_anonymous:
                user = authenticate(request=request)
                if user is not None:
                    request.user = request._cached_user = user
        except (AttributeError, KeyError, ValueError):
            """Either the Authorization header was not found, or it was invalid."""
            pass

        response = self.get_response(request)

        return response
