from django.contrib.auth.backends import BaseBackend

from djangocities.users.models import User


class AuthTokenBackend(BaseBackend):
    """Authenticate user from a request's _auth_token property."""
    def authenticate(self, request, *_):
        try:
            username = request._auth_token["user"]
            return User.objects.get(username=username)
        except (User.DoesNotExist, AttributeError, KeyError):
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None
