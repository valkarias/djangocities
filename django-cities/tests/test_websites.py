class TestWebsiteModel:
    def test_first_website_address_is_one(self, alien_slugs):
        assert alien_slugs.address == 1

    # pass both slugs and kitchen fixtures, otherwise slugs site is not created and this test fails
    def test_second_site_address_is_two(self, alien_slugs, alien_kitchen):
        assert alien_kitchen.address == 2

    def test_first_site_second_city_address_is_one(self, alien_slugs, rock_land):
        assert rock_land.address == 1
