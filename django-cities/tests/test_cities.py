class TestCityModel:
    def test_city_name(self, area51):
        assert area51.name == "Area 51"

    def test_city_slug(self, area51):
        assert area51.slug == "area-51"
