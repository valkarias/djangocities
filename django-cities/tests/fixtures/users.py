import pytest

from djangocities.users.models import User


@pytest.fixture
def admin_user(db) -> User:
    return User.objects.create_superuser("admin", "admin@example.com", "letmeadmin")


@pytest.fixture
def regular_user(db) -> User:
    return User.objects.create_user("regular", "regular@example.com", "letmelogin")
