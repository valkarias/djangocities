import pytest

from djangocities.cities.models import WebSite


@pytest.fixture
def alien_slugs(db, area51, regular_user) -> WebSite:
    return WebSite.objects.create(
        city=area51, owner=regular_user, name="Alien Slugs ate my Brains!",
    )


@pytest.fixture
def alien_kitchen(db, area51, regular_user) -> WebSite:
    return WebSite.objects.create(city=area51, owner=regular_user, name="Alien Kitchen")


@pytest.fixture
def rock_land(db, sunset_strip, regular_user) -> WebSite:
    return WebSite.objects.create(
        city=sunset_strip, owner=regular_user, name="Rock Land"
    )
