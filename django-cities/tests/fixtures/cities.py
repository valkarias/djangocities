import pytest

from djangocities.cities.models import City


@pytest.fixture
def area51(db) -> City:
    return City.objects.create(
        name="Area 51", description="Space in cyberspace", slug="area-51",
    )


@pytest.fixture
def sunset_strip(db) -> City:
    return City.objects.create(
        name="Sunset Strip", description="Loudest city of all", slug="sunset-strip",
    )
