# These are pytest fixtures, they need to be imported
# (Though I could admittedly also add an __all__ instead of noqa)
# flake8: noqa
from .cities import area51, sunset_strip
from .users import admin_user, regular_user
from .websites import alien_slugs, alien_kitchen, rock_land
